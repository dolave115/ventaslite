<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
//use App\Traits\CartTrait;
class ProductsController extends Component
{
    use WithPagination;
	use WithFileUploads;
//	use CartTrait;
#TODO:SELECT_ID POR DEFECTO EMPIEZA EN 0 Y POSTERIORMENTE AL SER CARGADO EL ID CAMBIA A >0 Y SE EJECUTA EDITAR
    public $name,$search,$image,$selected_id,$stock,$pageTitle,$componentName,$categoryid,$alerts,$price,$cost,$barcode;
    private $pagination=5; 
    #TODO:primer metodo que se ejecutra en un componente es para inicializar data
    public function mount(){
        $this->pageTitle='Listado';
        $this->componentName='Productos';
        $this->categoryid='Elegir';
    }
    public function paginationView(){
        return 'vendor.livewire.bootstrap';
    }
    public function render()
    {   // use WithFileUploads;
        #TODO: Se realiza validación del buscador "search" para saber si están buscando, search está creado en common search, en el formulario
        if (strlen($this->search)>0) {
            # code...
            #TODO:hace el joiin filtrando por el category_id que existe en ambas tablas
            $products=Product::join('categories as c', 'c.id', 'products.category_id')
            #TODO: Filtrar la categoria correspondiente a cada producto
            ->select('products.*', 'c.name as category')
            #TODO:realiza tres búsquedas al mismo tiempo
            ->where('products.name', 'like', '%' . $this->search . '%')
            ->orWhere('products.barcode', 'like', '%' . $this->search . '%')
            ->orWhere('c.name', 'like', '%' . $this->search . '%')
            #TODO:Organiza los productos de forma ascendente
            ->orderBy('products.name', 'asc')
            ->paginate($this->pagination);

        }
        else 
        $products = Product::join('categories as c', 'c.id', 'products.category_id')
                    ->select('products.*', 'c.name as category')
                    ->orderBy('products.name', 'asc')
                    ->paginate($this->pagination);

        #TODO:organizar información y enviarla al front
        $data=Product::orderBy('id','desc')->paginate($this->pagination);
        return view('livewire.products.products',['data'=>$products,'categories' => Category::orderBy('name', 'asc')->get()])
        ->extends('layouts.themes.app')
        ->section('content');
    }
    public function resetUI()
	{
		$this->name = '';
		$this->barcode = '';
		$this->cost = '';
		$this->price = '';
		$this->stock = '';
		$this->alerts = '';
		$this->search = '';
		$this->categoryid = 'Elegir';
		$this->image = null;
		$this->selected_id = 0;
		$this->resetValidation();
	}
    public function Store(){
        #TODO:Reglas para las validaciones que se hacen en el formulario,asi se pueden detectar
        $rules  = [
			'name' => 'required|unique:products|min:3',
			'cost' => 'required',
			'price' => 'required',
			'stock' => 'required',
			'alerts' => 'required',
			'categoryid' => 'required|not_in:Elegir'
		];
        #TODO:MENSAJES EN BASE A LAS REGLAS
		$messages = [
			'name.required' => 'Nombre del producto requerido',
			'name.unique' => 'Ya existe el nombre del producto',
			'name.min' => 'El nombre del producto debe tener al menos 3 caracteres',
			'cost.required' => 'El costo es requerido',
			'price.required' => 'El precio es requerido',
			'stock.required' => 'El stock es requerido',
			'alerts.required' => 'Ingresa el valor mínimo en existencias',
			'categoryid.not_in' => 'Elige un nombre de categoría diferente de Elegir',
		];

		$this->validate($rules, $messages);
        #TODO:Llena de manera masiva la base de datos con los wire.model
        $product = Product::create([
			'name' => $this->name,
			'cost' => $this->cost,
			'price' => $this->price,
			'barcode' => $this->barcode,
			'stock' => $this->stock,
			'alerts' => $this->alerts,
			'category_id' => $this->categoryid
		]);
            #TODO:guarda la imagen con un id unico concatenado a la extension
		if ($this->image) {
			$customFileName = uniqid() . '_.' . $this->image->extension();
            #TODO:Guardar como 
			$this->image->storeAs('public/products', $customFileName);
            #TODO: asigna a la columna imagen el nombre
			$product->image = $customFileName;
            #TODO:se guarda en el modelo de bases de datos
			$product->save();
		}

		$this->resetUI();
		$this->emit('product-added', 'Producto Registrado');
	}
    public function Edit(Product $product)
	{
		$this->selected_id = $product->id;
		$this->name = $product->name;
		$this->barcode = $product->barcode;
		$this->cost = $product->cost;
		$this->price = $product->price;
		$this->stock = $product->stock;
		$this->alerts = $product->alerts;
		$this->categoryid = $product->category_id;
		$this->image = null;

		$this->emit('modal-show', 'Show modal');
	}

	public function Update()
	{
		$rules  = [
            
			'name' => "required|min:3|unique:products,name,{$this->selected_id}",
			'cost' => 'required',
			'price' => 'required',
			'stock' => 'required',
			'alerts' => 'required',
			'categoryid' => 'required|not_in:Elegir'
		];

		$messages = [
			'name.required' => 'Nombre del producto requerido',
			'name.unique' => 'Ya existe el nombre del producto',
			'name.min' => 'El nombre del producto debe tener al menos 3 caracteres',
			'cost.required' => 'El costo es requerido',
			'price.required' => 'El precio es requerido',
			'stock.required' => 'El stock es requerido',
			'alerts.required' => 'Ingresa el valor mínimo en existencias',
			'categoryid.not_in' => 'Elige un nombre de categoría diferente de Elegir',
		];

		$this->validate($rules, $messages);
		#TODO:se buscan los parametros por id
		$product = Product::find($this->selected_id);
		#TODO:se actualizan los parametros
		$product->update([
			'name' => $this->name,
			'cost' => $this->cost,
			'price' => $this->price,
			'barcode' => $this->barcode,
			'stock' => $this->stock,
			'alerts' => $this->alerts,
			'category_id' => $this->categoryid
		]);
		#TODO:Se evalua la imagen del formulario y se borra si esta repetida
		if ($this->image) {
			$customFileName = uniqid() . '_.' . $this->image->extension();
			$this->image->storeAs('public/products', $customFileName);
			$imageTemp = $product->image; // imagen temporal
			$product->image = $customFileName;
			$product->save();
			#TODO:Se verifica si en la columna image de la tabla está nulo, si no lo está se borra dato
			if ($imageTemp != null) {
				if (file_exists('storage/products/' . $imageTemp)) {
					unlink('storage/products/' . $imageTemp);
				}
			}
		}

		$this->resetUI();
		$this->emit('product-updated', 'Producto Actualizado');
	}
	protected $listeners=['deleteRow'=>'Destroy'];
	public function Destroy(Product $product){
		$imageTemp=$product->image;
		$product->delete();
		if($imageTemp!=null){
			if (file_exists('storage/products/'.$imageTemp)) {
				# code...
				unlink('storage/products/'.$imageTemp);
			}
		}
		$this->resetUI();
		$this->emit('product-deleted','Producto Eliminado');
	}


    }
