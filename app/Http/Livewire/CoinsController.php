<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Denominations;
use Illuminate\Support\Facades\Storage;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
class CoinsController extends Component
{
    public $componentName,$pageTitle,$selected_id,$image,$search,$type,$value,$pagination;
    use WithPagination;
	use WithFileUploads;
    public function paginationView(){
        return 'vendor.livewire.bootstrap';
    }
    public function mount(){
        $this->componentName='Denominaciones';
        $this->pageTitle='Listado';
        $this->type='Elegir';
        $this->pagination=5;
        $this->selected_id=0;

    }
    public function render()
    {
    
            # code...
            if(strlen($this->search)>0)
            #TODO:Metodo where=Nombre de la columna,operador(en este caso like),operador para buscar letra en columna
            $data=Denominations::query()
            ->where('type','like','%'.$this->search.'%')
            ->orWhere('denominations.value', 'like', '%' . $this->search . '%')
            ->paginate($this->pagination);
        else
            $data=Denominations::orderBy('id','desc')->paginate($this->pagination);
        
        return view('livewire.denominations.coins',[
            'data'=>$data
        ])
        ->extends('layouts.themes.app')
        ->section('content');
    }
    public function Edit($id)
    {
        $record=Denominations::find($id,['id','type','value','image']);
        $this->type=$record->type;
        $this->value=$record->value;
        $this->selected_id=$record->id;
        $this->image=null;
        $this->emit('show-modal','show modal!'); 
    }
    public function Store(){
        $rules=[
            'type'=>'required|not_in:Elegir',
            'value'=>'required|unique:denominations'
        ];
        $messages=[
            'type.required'=>'El tipo es requerido',
            'type.not_in'=>'Elige un valor para el tipo distinto a Elegir',
            'value.required'=>'El valor es requerido'
        ];
        $this->validate($rules,$messages);
        $denominations=Denominations::create([
            'type'=>$this->type,
            'value'=>$this->value
        ]);
        
        if($this->image)
        {
            $customFileName=uniqid().'_.'.$this->image->extension();
            $this->image->storeAs('public/denominations',$customFileName);
            $denominations->image=$customFileName;
            $denominations->save();
        }
        $this->resetUI();
        $this->emit('denomination-added','Denominaición Registrada');
    }
    public function Update()
    {
    	$rules =[
    		'type' => 'required|not_in:Elegir',
            #TODO: Valor único en la tabla sobre value y seleccionando el id
    		'value' => "required|unique:denominations,value,{$this->selected_id}"
    	];

    	$messages =[
    		'type.required' => 'El tipo es requerido',
    		'type.not_in' => 'Elige un tipo válido',
    		'value.required' => 'El valor es requerido',
    		'value.unique' => 'El valor ya existe'
    	];

    	$this->validate($rules, $messages);


    	$denomination = Denominations::find($this->selected_id);
    	$denomination->update([
    		'type' => $this->type,
    		'value' => $this->value
    	]);

    	if($this->image)
    	{
    		$customFileName = uniqid() . '_.' . $this->image->extension();
    		$this->image->storeAs('public/denominations', $customFileName);
    		$imageName = $denomination->image;

    		$denomination->image = $customFileName;
    		$denomination->save();

    		if($imageName !=null)
    		{
    			if(file_exists('storage/denominations' . $imageName))
    			{
    				unlink('storage/denominations' . $imageName);
    			}
    		}

    	}

    	$this->resetUI();
    	$this->emit('denomination-updated', 'Denominación Actualizada');
    }
    public function resetUI() 
    {
    	$this->type ='';
    	$this->value ='';
    	$this->image = null;
    	$this->search ='';
    	$this->selected_id =0;
    }


	protected  $listeners = [
		'deleteRow' => 'Destroy'
	];

    public function Destroy(Denominations $denominations)
    {   	
    	
    	$imageName = $denominations->image; 
    	$denominations->delete();

    	if($imageName !=null) {
    		unlink('storage/denominations/' . $imageName);
    	}

    	$this->resetUI();
    	$this->emit('denomination-deleted', 'Denominación Eliminada');

    }



}
