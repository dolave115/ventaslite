<?php

namespace App\Http\Livewire;
use Darryldecode\Cart\Facades\CartFacade as Cart;
use Livewire\Component;
use App\Models\Denominations;
use App\Models\Product;
use App\Models\Sale;
use DB;
class PosController extends Component
{   public $total=0,$itemsQuantity=1,$efectivo,$change;

    public function mount(){
        $this->efectivo=0;
        $this->change=0;
        $this-> total=Cart::getTotal();
        $this-> itemsQuantity=Cart::getTotalQuantity();
    }
    public function render()
    {   
        return view('livewire.pos.pos',[
            'denominations'=>Denominations::orderBy('value','desc')->get(),
            'cart'=>Cart::getContent()->sortBy('name')
        ])
        ->extends('layouts.themes.app')
        ->section('content');
    }
    public function ACash($value)
    {
        $this->efectivo+=($value==0 ? $this->total:$value);
        $this->change=($this-> efectivo- $this->total); 
    }
    protected $listeners=[
        'scan-code'=>'ScanCode',
        'removeItem'=>'removeItem',
        'clearCart'=>'clearCart',
        'saveSale'=>'saveSale'
    ];
    public function ScanCode($barcode,$cant=1)
    {   //dd($barcode);
        $product=Product::where('barcode',$barcode)->first();
        if($product == null || empty($product))
        {
            $this->emit('scan-notfound','El producto no está registrado');
        }else{
            if($this->InCart($product->id))
            {
                $this->increaseQty($product->id);
                return;
            }
            if($product->stock<1)
            {
                $this->emit('no-stock','Stock insuficiente :/');
                return;
            }
            Cart::add($product->id,$product->name,$product->price,$cant,$product->image);
            $this->total=Cart::getTotal();
            $this->emit('scan-ok','Producto agregado');
        }

    }
    public function InCart($productId){
        #TODO:Se extrae del carrito con el product ID, si existe retorna verdadero
        $exist=Cart::get($productId);
       $exist?  true:false;
        
    }
    public function increaseQty($productId,$cant=1){
        $title='';
        #TODO:se extrae de la tabla producto el producto con el id
        $product=Product::find($productId);
        #TODO:Obtener existencia de producto dentro del carro
        $exist=Cart::get($productId);
        #TODO:Verificar si existe en carrito y emitir algún mensaje de acuerdo a la previa validación
        if($exist)
            $title='Cantidad actualizada';
        else
            $title='Producto agregado';

        #TODO:Validar si la cantidad en stock es menor a la cantidad que hay en el carrito
        if($exist)
        {
            if($product->stock<($cant+$exist->quantity))
            {
                $this->emit('no-stock','Stock insuficiente');
                return;
            }
        } 
        #TODO:agregar al carrito usando el método add
        Cart::add($product->id,$product->name,$product->price,$cant,$product->image);
        $this->total=Cart::getTotal();
        $this->itemsQuantity=Cart::getTotalQuantity();
        $this->emit('scan-ok',$title);     
    }
    public function updateQty($productId,$cant=1){
        $title='';
        #TODO:se extrae de la tabla producto el producto con el id
        $product=Product::find($productId);
        #TODO:Obtener existencia de producto dentro del carro
        $exist=Cart::get($productId);
        #TODO:Verificar si existe en carrito y emitir algún mensaje de acuerdo a la previa validación
        if($exist)
            $title='Cantidad actualizada';
        else
            $title='Producto agregado';
        #TODO:Validar si la cantidad en stock es menor a la cantidad que hay en el carrito
        if($exist)
        {
            if($product->stock<$cant)
            {
                $this->emit('no-stock','Stock insuficiente');
                return;
            }
        }

        $this->removeItem($productId);
        if($cant>0)
        {
            Cart::add($product->id,$product->name,$product->price,$cant,$product->image);
            $this->total=Cart::getTotal();
            $this->itemsQuantity=Cart::getTotalQuantity();
            $this->emit('scan-ok',$title);     
        }
        else{
            $this->emit('scan-ok','cantidad debe ser mayor a cero');   
        }
    }
    public function removeItem($productId){
        #TODO:BORRA ITEM
        Cart::remove($productId);
        $this->total=Cart::getTotal();
        $this->itemsQuantity=Cart::getTotalQuantity();
        $this->emit('scan-ok','Producto eliminado');
    }
    public function decreaseQty($productId){
        #TODO:JALA el producto dentro del carrito guardando un backup para despues reguardarlo
        $item=Cart::get($productId);
        Cart::remove($productId);
        #TODO:resta cantidad 
        $newQty=($item->quantity)-1;
        #TODO:GUarda el nuevo producto
        if($newQty >0)
            Cart::add($item->id,$item->name,$item->price,$newQty,$item->attributes[0]);
        #TODO:obtiene la información para el front
        $this->total=Cart::getTotal();
        $this->itemsQuantity=Cart::getTotalQuantity();
        $this->emit('scan-ok','Cantidad actualizada');
    }
    public function clearCart(){
        Cart::clear();
        #TODO:borrar carrito
        $this->efectivo=0;
        $this->change=0;
        $this->total=Cart::getTotal();
        $this->itemsQuantity=Cart::getTotalQuantity();

        $this->emit('scan-ok','Carrito vacío');
    }
    public function saveSale(){
        if($this->total <=0)
        {

            $this->emit('sale-error','Agrega productos a la venta');
            return;
        }
        if($this->total <=0){
            $this->emit('sale-error','Ingrese efectivo ');
            return;
        }
        if($this->total > $this->efectivo){
            $this->emit('sale-error','Ingrese más efectivo ');
            return;
        }
        DB::beginTransaction();
        try {
            $sale=Sale::create([
                'total'=>$this->total,
                'items'=>$this->itemsQuantity,
                'cash'=>$this->efectivo,
                'change'=>$this->change,
                'user_id'=>Auth()->user()->id
            ]);
            if($sale){
                $items=Cart::getContent();
                foreach ($variable as $item) {
                    SaleDetail::create([
                        'price'=>$item->price,
                        'quantity'=>$item->quantity,
                        'product_id'=>$item->id,
                        'sale_'=>$sale->id,
                    ]);
                    #TODO:Actualizar stock
                    $product=Product::find($item->id);
                    $product->stock=$product->stock - $item->quantity;
                    $product->save();
                }
            }
            #TODO:actualizar base de datos
            DB::commit();
            #TODO:Borrar carrito
            Cart::Clear();
            #TODO:EFECTIVO EN 0
            $this->efectivo=0;
            $this->change=0;
            $this-> total=Cart::getTotal();
            $this->itemsQuantity=Cart::getTotalQuantity();
            #TODO:ENVIAR EVENTOS e imprimir ID
            $this->emit('sale-ok','Venta registrada con éxito');
            $this->emit('print-ticket',$sale->id);
        } catch (Exception $e) {
            #TODO:Deshacer cambios en base de datos
            DB::rollback();
            #TODO: Obtener e imprimir mensaje de error
            $this->emit('sale-error',$e->getMessage());

        }


    }
    public function printTicket($sale)
    {
        return Redirect::to("print://$sale->id");
    }

}
