<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class CategoriesController extends Component
{
    use WithFileUploads;
    use WithPagination;
    public $name,$search,$image,$selected_id,$pageTitle,$componentName;
    private $pagination=5;
    public function mount(){
        $this->pageTitle='Listado';
        $this->componentName='Categorías';
    }
    public function paginationView(){
        return 'vendor.livewire.bootstrap';
    }
    public function render()
    {
        if(strlen($this->search)>0)
            #TODO:Metodo where=Nombre de la columna,operador(en este caso like),operador para buscar letra en columna
            $data=Category::query()
            ->where('name','like','%'.$this->search.'%')->paginate($this->pagination);
        else
            $data=Category::orderBy('id','desc')->paginate($this->pagination);
        return view('livewire.category.categories',['categories'=>$data])
        ->extends('layouts.themes.app')
        ->section('content');
    }
    public function Edit($id)
    {
        $record=Category::find($id);
        $this->name=$record->name;
        $this->selected_id=$record->id;
        $this->image=null;
        $this->emit('show-modal','show modal!'); 
    }
    public function Store(){
        $rules=[
            'name'=>'required|unique:categories|min:3'
        ];
        $messages=[
            'name.required'=>'Nombre de la categoría es requerido',
            'name.unique'=>'Ya existe el nombre de la categoría',
            'name.min'=>'El nombre de la categoría debe tener al menos 3 caracteres'
        ];
        $this->validate($rules,$messages);
        $category=Category::create(['name'=>$this->name]);
        $customfileName;
        if ($this->image) {
            # code...
            #TODO:funcion para asignar id único y anexarle la extensión que ingresó el usuario
            $customfileName=uniqid().'_.'.$this->image->extension();
            $this->image->storeAs('public/categories',$customfileName);
            $category->image=$customfileName;
            $category->save();
        }
        $this->resetUI();
        #TODO:Emitir llamado para realizar la acción de 
        $this->emit('category-added','Categoria Registrada');
    }
    public function Update(){
        $rules=[
            #TODO: Filtra el nombre de la categoría y activa el mensaje por el unique
            'name'=>'required|unique:categories|min:3,name,{$this->selected_id}'
        ];
        $messages=[
            'name.required'=>'Nombre de la categoría es requerido',
            'name.unique'=>'Ya existe el nombre de la categoría',
            'name.min'=>'El nombre de la categoría debe tener al menos 3 caracteres'
        ];
        #TODO:Validar las reglas
        $this->validate($rules,$messages);
        #TODO: Buscar dentro del Category(tabla) con el id
        $category=Category::find($this->selected_id);
        #TODO:Actualizar el nombre con name del formulario
        $category->update([
            'name'=>$this->name
        ]);
        #TODO:Validar imagenes
        if($this->image)
        {
            #TODO:Crear nombre unico
            $customfileName=uniqid().'_.'.$this->image->extension();
            $this->image->storeAs('public/categories',$customfileName);
            $imageName=$category->image;
            #TODO:Recuperar nombre anterior
            $category->image=$customfileName;
            $category->save();
            #TODO:Validar si imagen es nulla
            if($imageName!=null)
            {#TODO:Si el archivo existe lo borrara
                if(file_exists('storage/categories'.$imageName))
                {
                    unlink('storage/categories'.$imageName);
                }
            }
        }
        $this->resetUI();
        #TODO:Emitir llamado para realizar la acción de 
        $this->emit('category-updated','Categoria Actualizada ');

    }
     public function resetUI(){
            $this->name='';
            $this->image=null;
            $this->search='';
            $this->selected_id=0;
     }
     protected $listeners=[
        'deleteRow'=>'Destroy'
     ];
     public function Destroy(Category $category){
        //$category=Category::find($id);
        //dd($category);
        $imageName=$category->image;
        $category->delete();
        if($imageName!=null){
            unlink('storage/categories/'.$imageName);
        }
        $this->resetUI();
        $this->emit('category-deleted','Categoria Eliminada1');
     }
}
