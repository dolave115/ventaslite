<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'Diego Olave',
           
            'email'=>'dolave115@gmail.com',
            'profile'=>'ADMIN',
            'status'=>'ACTIVE',
            'password'=> bcrypt('123')
        ]);
        User::create([
            'name'=>'Asistente 01',
            
            'email'=>'asistente@gmail.com',
            'profile'=>'EMPLOYEE',
            'status'=>'ACTIVE',
            'password'=> bcrypt('123')
        ]);
    }
}
