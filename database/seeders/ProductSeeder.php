<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name'=>'DISPENSADOR',
            'cost'=>200,
            'price'=>350,
            'barcode'=>'1018493059',
            'stock'=>1000,
            'alerts'=>10,
            'category_id'=>1,
            'image'=> 'curso.png'
        ]);
        Product::create([
            'name'=>'DESAROLLO WEB',
            'cost'=>200,
            'price'=>350,
            'barcode'=>'1018493050',
            'stock'=>1000,
            'alerts'=>10,
            'category_id'=>2,
            'image'=> 'curso.png'
        ]);
        Product::create([
            'name'=>'MODULO DE TEMPERATURA',
            'cost'=>200,
            'price'=>350,
            'barcode'=>'1018493051',
            'stock'=>1000,
            'alerts'=>10,
            'category_id'=>3,
            'image'=> 'curso.png'
        ]);
        Product::create([
            'name'=>'MODULO DE TIEMPO',
            'cost'=>200,
            'price'=>350,
            'barcode'=>'1018493052',
            'stock'=>1000,
            'alerts'=>10,
            'category_id'=>4,
            'image'=> 'curso.png'
        ]);
    }
}
