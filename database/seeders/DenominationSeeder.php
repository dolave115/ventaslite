<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Denominations;
class DenominationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Denominations::create([
            'type'=>'BILLETE',
            'value'=> 100000
        ]);
        Denominations::create([
            'type'=>'BILLETE',
            'value'=> 50000
        ]);
        Denominations::create([
            'type'=>'BILLETE',
            'value'=> 20000
        ]);
        Denominations::create([
            'type'=>'BILLETE',
            'value'=> 10000
        ]);
        Denominations::create([
            'type'=>'BILLETE',
            'value'=> 5000
        ]);
        Denominations::create([
            'type'=>'BILLETE',
            'value'=> 2000
        ]);
        Denominations::create([
            'type'=>'MONEDA',
            'value'=> 1000
        ]);
        Denominations::create([
            'type'=>'MONEDA',
            'value'=> 500
        ]);
        Denominations::create([
            'type'=>'MONEDA',
            'value'=> 200
        ]);
        Denominations::create([
            'type'=>'MONEDA',
            'value'=> 100
        ]);
        Denominations::create([
            'type'=>'MONEDA',
            'value'=> 50
        ]);
        Denominations::create([
            'type'=>'OTRO',
            'value'=> 200
        ]);
    }
    }

