<!-- #TODO:wire:ignore.self es para no actualizar el modal, pero si sus hijos -->
<!-- #TODO:wire:Pie de modal, común a todos -->
<div wire:ignore.self class="modal fade" id="theModal"tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-dark">
        <h5 class="modal-title text-white">
            <b>{{$componentName}}</b>|{{$selected_id>0 ?'EDITAR':'CREAR'}}
        </h5>
        <h6 class="text-center text-warning" wire:loading>POR FAVOR ESPERE</h6>
      </div>
      <div class="modal-body">

<div class="row">
<div class="col-sm-12">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <span class="fas fa-edit">

                    </span>
                </span>
            </div>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <input type="text" wire:model.lazy="roleName" class="form-control" placeholder="ej: Category" maxlength="255">
        </div>
        @error('roleName')<span class="text-danger er">{{$message}}</span>@enderror
    </div>
</div>
      <!-- #TODO:wire:click.prevent usado para establecer una función al hacer click en el boton -->
<!-- #TODO:footer comun a todos del modal -->
<!-- #TODO:Evento resetUI() asociado a renderizar de nuevo la vista categorias -->
</div>
      <div class="modal-footer">
        <button type="button" wire:click.prevent="resetUI()" class="btn btn-dark close btn textinfo"style="background:#3BF5C1" data-dismiss="modal">CERRAR</button>
        @if($selected_id<1)
        <button type="button" wire:click.prevent="CreateRole()" class="btn btn-dark close-modal">GUARDAR</button>
        @else
        <!-- #TODO:remitirnos al metodo para realizar acción al presionar el boton -->
        <button type="button" wire:click.prevent="UpdateRole()" class="btn btn-dark close-modal">ACTUALIZAR</button>
        @endif
      </div>
    </div>
  </div>
</div>