
@include('common.modalHead')
<div class="row">
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label>Nombre</label>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <input type="text" wire:model.lazy="name" class="form-control product-name" placeholder="ej: Curso Laravel" autofocus>
        </div>
        @error('name')<span class="text-danger er">{{$message}}</span>@enderror
    </div>
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label>Código</label>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <input type="text"data-type="currency" wire:model.lazy="barcode" class="form-control" placeholder="ej:234589666">
        </div>
        @error('barcode')<span class="text-danger er">{{$message}}</span>@enderror
    </div>
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label>Costo</label>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <input type="text" data-type="currency"wire:model.lazy="cost" class="form-control" placeholder="ej:0.00">
        </div>
        @error('cost')<span class="text-danger er">{{$message}}</span>@enderror
    </div>
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label>Precio</label>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <input type="text" data-type="currency"wire:model.lazy="price" class="form-control" placeholder="ej:0.00">
        </div>
        @error('price')<span class="text-danger er">{{$message}}</span>@enderror
    </div>
    
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label>Stock</label>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <input type="number" data-type="currency"wire:model.lazy="stock" class="form-control" placeholder="ej:0">
        </div>
        @error('stock')<span class="text-danger er">{{$message}}</span>@enderror
    </div>
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label>Alertas</label>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <input type="number" wire:model.lazy="alerts" class="form-control" placeholder="ej:10">
        </div>
        @error('alerts')<span class="text-danger er">{{$message}}</span>@enderror
    </div>
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label>Categorias</label>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <select class="form-control" wire:model='categoryid'>
                <option value="Elegir">Elegir</option>
                @foreach($categories as $category)
                <option value="{{$category->id}}" >"{{$category->name}}"</option>
                @endforeach
            </select>
            @error('categoryid')<span class="text-danger er">{{$message}}</span>@enderror
        </div>
        
    </div>
    <<div class="col-sm-12 col-md-8">
<div class="form-group custom-file">
	<input type="file" class="custom-file-input form-control" wire:model="image"
	accept="image/x-png, image/gif, image/jpeg"  
	 >
	 <label class="custom-file-label">Imágen {{$image}}</label>
	 @error('image') <span class="text-danger er">{{ $message}}</span>@enderror
</div>
</div>

</div>

@include('common.modalFooter')