<div class="row sales layout-top-spacing">
<div class="col-sm-12">
    <div class="widget widget-chart-one">
        <div class="widget-heading">
            <h4 class="card-title">
                <!-- #TODO: COMPONENTES DE NOMBRES Y TITULO DE PÁGINA -->
                <b>{{$componentName}}|{{$pageTitle}}</b>
            </h4>
            <ul class="tabs tab-pills">
                <li>
                    <a href="javascript:void(0)" class="tabmenu bg-dark" data-toggle="modal" data-target="#theModal">Agregar</a>
                </li>
                
            </ul>
        </div>
        <!-- #TODO: BUSCADOR -->
        @include('common.searchbox')
        <div class="widget-content">
            <div class="table-responsive">
                <table class="table table bordered table striped mt-1">
                    <thead class="text-white" style="background:#3BF5C">
                        <tr>
                                <th class="table-th text-white">DESCRIPCIÓN</th>
								<th class="table-th text-white text-center">BARCODE</th>
								<th class="table-th text-white text-center">CATEGORÍA</th>
								<th class="table-th text-white text-center">PRECIO</th>
								<th class="table-th text-white text-center">STOCK</th>
								<th class="table-th text-white text-center">INV.MIN</th>
								<th class="table-th text-white text-center">IMAGEN</th>
								<th class="table-th text-white text-center">ACTIONS</th>
                        </tr>

                    </thead>
                    <tbody>
                        <!-- #TODO:FOREACH QUE RECORRE LA INFORMACION Y LLENA LAS TABLAS -->
                        @foreach($data as $product)
                        <tr>
                                <td><h6 class="text-left">{{$product->name}}</h6></td>
								<td><h6 class="text-center">{{$product->barcode}}</h6></td>
								<td><h6 class="text-center">{{$product->category}}</h6></td>
								<td><h6 class="text-center">{{$product->price}}</h6></td>
								<td>
									<h6 class="text-center {{$product->stock <= $product->alerts ? 'text-danger' : '' }} ">
										{{$product->stock}}
									</h6>
								</td>
								<td><h6 class="text-center">{{$product->alerts}}</h6></td>
                           
                            <td class="text-center">
                                <span><img src="{{ asset('storage/products/' . $product->imagen2 ) }}" alt="imagen de ejemplo" height="70" width="80" class="rounded"></span>
                            </td>
                            <td class="text-center">
                                
                                <a href="javascript:void(0)"wire:click="Edit('{{$product->id}}')" class="btn btn-dark mtmobile" title="Edit"><i class="fas fa-edit"></i></a>
                                <a href="javascript:void(0)"onclick="Confirm('{{$product->id}}')" class="btn btn-dark" title="Delete"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              
                {{$data->links()}}
            </div>
        </div>
    </div>
    <!-- #TODO:AGREGAR FORMULARIO -->
  
</div>

@include('livewire.products.form')
</div>
<script>
        document.addEventListener('DOMContentLoaded',function(){
        window.livewire.on('product-added',msg=>{
             $('#theModal').modal('hide');
             noty(msg)
        })
        window.livewire.on('product-updated',msg=>{
             $('#theModal').modal('hide');
             noty(msg)
        })
        window.livewire.on('product-deleted',msg=>{
          
             noty(msg)
        })
        window.livewire.on('modal-hide',msg=>{
             $('#theModal').modal('hide');
          
        })
        window.livewire.on('modal-show',msg=>{
             $('#theModal').modal('show');
            
        })
        // #TODO: eventos vinculados a los errores que podrían darse en el formulario
        window.livewire.on('hidden.bs.modal',msg=>{
             $('.er').modal('display','none');
            
        })
    });
function Confirm(id)
{
    
    swal({
        title:'CONFIRMAR',
        text:'¿CONFIRMAS ELIMINAR EL REGISTRO',
        type:'success',
        showCancelButton:true,
        cancelButtonText:'Cerrar',
        cancelButtonColor:'#fff',
        confirmButtonText:'Aceptar',
        confirmButtonColor:'#3B3F5C'
    }).then(function(result){
        if(result.value){
            window.livewire.emit('deleteRow',id)
            swal.close()
        }
    })
}
</script>