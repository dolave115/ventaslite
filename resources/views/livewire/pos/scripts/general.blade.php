<script>
    document.addEventListener('DOMContentLoaded', function() {
    $('.tblscroll').nicescroll({
        cursoscolor:"#515365",
        cursorwidth:"30px",
        background:"rgba(20,20,20,0.3)",
        cursorborder:"0px",
        cursorborderradius:3 
    })
})
function Confirm(id,eventName,text)
{
    if(products>0){
        swal('NO SE PUEDE ELIMINAR LA CATEGORIA PORQUE TIENE PRODUCTOS RELACIONADOS')
        return;
    }
    swal({
        title:'CONFIRMAR',
        text:text,
        type:'success',
        showCancelButton:true,
        cancelButtonText:'Cerrar',
        cancelButtonColor:'#fff',
        confirmButtonText:'Aceptar',
        confirmButtonColor:'#3B3F5C'
    }).then(function(result){
        if(result.value){
            window.livewire.emit(eventName,id)
            swal.close()
        }
    })
}
</script>