<div class="row mt-3">
    <!-- #TODO:Columnas en todas las resoluciones -->
	<div class="col-sm-12">
        <!-- #TODO:recuadro gris -->
		<div class="connect-sorting">
            <!-- #TODO:Titutlo -->
			<h5 class="text-center mb-2">DENOMINACIONES</h5>
            <!-- #TODO:Dentro de un container se pueden dibujar botones -->
			<div class="container">
            <!-- #TODO:COLUMNA -->
				<div class="row">
					@foreach($denominations as $d)

					<div class="col-sm mt-2">
                        <!-- #TODO:boton de denominaciones 500,100... enviado a una función,
                        cada boton clickeado enviará la información por medio de la directiva wire -->
						<button wire:click.prevent="ACash({{$d->value}})" class="btn btn-dark btn-block den">
                            <!-- #TODO:dicha condicional es útil para decidir si el valor que viene es 0,
                             si no lo es concatena $ con numero de formato y remplaza el punto por espacio vacio, si es 0 solo pasa 
                            la cadena 'Exacto' -->
							{{ $d->value >0 ? '$' . number_format($d->value,2, '.', '') : 'Exacto' }}
						</button>
					</div>
					@endforeach
				</div>
			</div>

			<div class="connect-sorting-content mt-4">
				<div class="card simple-title-task ui-sortable-handle">
					<div class="card-body">
						<div class="input-group input-group-md mb-3">
							<div class="input-group-prepend">
                                <!-- #TODO:Se oculta en resoluciones pequeñas -->
								<span class="input-group-text input-gp hideonsm" style="background: #3B3F5C; color:white">Efectivo F8
								</span>
							</div>
                            <!-- #TODO:Entrada que queda al frente del efectivo -->
                            <!-- #TODO:Al dar entener ejecuta metodo saveSale -->
							<input type="number" id="cash" wire:model="efectivo" wire:keydown.enter="saveSale" class="form-control text-center" value="{{$efectivo}}">
							<!-- #TODO:ES LA X LIMPIA LA INPUT -->
							<div class="input-group-append">
								<span wire:click="$set('efectivo', 0)" class="input-group-text" style="background: #3B3F5C; color:white">
									<i class="fas fa-backspace fa-2x"></i>
								</span>
							</div>
						</div>
						<!-- #TODO:ASIGNAR CAMBIO CON LA VARIABLE CHANGE -->
						<h4 class="text-muted">Cambio: ${{number_format($change,2)}}</h4>
						<!-- #TODO:usando contenedor que mantiene la distancia con un margin top5 -->
						<div class="row justify-content-between mt-5">
							<!-- #TODO:Usando diferentes relaciones de tamaño dependiendo del dispositivo -->
							<div class="col-sm-12 col-md-12 col-lg-6">
								<!-- #TODO: si el total es mayor a 0 mostrara el boton de cancelar -->
								@if($total > 0)
								<button onclick="Confirm('','clearCart','¿SEGURO DE ELIMINAR EL CARRITO?')" class="btn btn-dark mtmobile">
									CANCELAR F4
								</button>
								@endif
							</div>
							<!-- #TODO:Si el efectivo es mayor o igual a total y total a su vez es mayor a 0 -->
							<div class="col-sm-12 col-md-12 col-lg-6">
								@if($efectivo>= $total && $total > 0)
								<button wire:click.prevent="saveSale" class="btn btn-dark btn-md btn-block">GUARDAR F6</button>
								@endif
							</div>


						</div>




					</div>
					<div class="col-sm-12 mt-1 text-center">
						<p class="text-muted">Reimprimir Última F7</p>
					</div>
				</div>
			</div>

		</div>

	</div>
</div>