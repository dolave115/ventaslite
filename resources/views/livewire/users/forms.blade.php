@include('common.modalHead')
<div class="row">
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label>Nombre</label>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <input type="text" wire:model.lazy="name" class="form-control" placeholder="ej: Diego Olave">
        </div>
        @error('name')<span class="text-danger er">{{$message}}</span>@enderror
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label>Teléfono</label>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <input type="text" wire:model.lazy="phone" class="form-control" placeholder="ej: 3137061498" maxlength="11">
        </div>
        @error('phone')<span class="text-danger er">{{$message}}</span>@enderror
    </div>
   
    <div class="col-sm-12 col-md-8">
        <div class="form-group">
            <label>Email</label>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <input type="text" wire:model.lazy="email" class="form-control" placeholder="ej: dolave115@gmail.com">
        </div>
        @error('email')<span class="text-danger er">{{$message}}</span>@enderror
    </div>
    
    <div class="col-sm-12 col-md-6">
        <div class="form-group">
            <label>Contraseña</label>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <input type="password" wire:model.lazy="password" class="form-control" >
        </div>
        @error('password')<span class="text-danger er">{{$message}}</span>@enderror
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="form-group">
            <label>Estado</label>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <select  wire:model.lazy="status" class="form-control">
                <option value="Elegir" selected>Elegir</option>
                <option value="Active" selected>Activo</option>
                <option value="Locked" selected>Bloqueado</option>
            </select>
        </div>
        @error('status')<span class="text-danger er">{{$message}}</span>@enderror
    </div>
  
    <div class="col-sm-12 col-md-6">
        <div class="form-group">
            <label>Asignar ROle</label>
            <!-- #TODO: con el wire:model.lazy="name" se puede capturar el valor del name -->
            <select  wire:model.lazy="profile" class="form-control">
                <option value="Elegir" selected>Elegir</option>
                @foreach($roles as $role )
                <option value="{{$role->name}}" selected>{{$role->name}}</option>
                @endforeach
            </select>
        </div>
        @error('profile')<span class="text-danger er">{{$message}}</span>@enderror
    </div>
    <div class="col-sm-12 col-md-8">
        <div class="form-group custom-file">
            <label> Imagen de Perfil</label>
            <input type="file" class=" form-control" wire:model="image"
            accept="image/x-png, image/gif, image/jpeg">
            
            @error('image') <span class="text-danger er">{{ $message}}</span>@enderror
        </div>
    </div>

</div>

@include('common.modalFooter')