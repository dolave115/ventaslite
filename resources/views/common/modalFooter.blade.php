<!-- #TODO:wire:click.prevent usado para establecer una función al hacer click en el boton -->
<!-- #TODO:footer comun a todos del modal -->
<!-- #TODO:Evento resetUI() asociado a renderizar de nuevo la vista categorias -->
</div>
      <div class="modal-footer">
        <button type="button" wire:click.prevent="resetUI()" class="btn btn-dark close btn textinfo"style="background:#3BF5C1" data-dismiss="modal">CERRAR</button>
        @if($selected_id<1)
        <button type="button" wire:click.prevent="Store()" class="btn btn-dark close-modal">GUARDAR</button>
        @else
        <!-- #TODO:remitirnos al metodo para realizar acción al presionar el boton -->
        <button type="button" wire:click.prevent="Update()" class="btn btn-dark close-modal">ACTUALIZAR</button>
        @endif
      </div>
    </div>
  </div>
</div>