<div class="row justify-content-between">
	
	<div class="col-lg-4 col-md-4 col-sm-12">
		
		<div class="input-group mb-4">
			<div class="input-group-prepend">
				<span class="input-group-text input-gp">
					<i class="fas fa-search"></i>
				</span>
			</div>
			<!-- #TODO: con el wire model search se puede ejecutar la búsqueda desde el controler -->
			<input type="text" wire:model="search" placeholder="Buscar" class="form-control" 
			>
		</div>

	</div>
</div>